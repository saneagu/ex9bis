package ro.orangeStartIT;

import java.util.*;

public class Main {
    // Create an application where you will demonstrate the use of the Interface Set.There are several
//classes implementing the interface, such as AbstractSet, EnumSet, HashSet, LinkedHashSet,
//TreeSet and ConcurrentSkipListSet. For this example we will use HashSet
    public static void main(String[] args) {

        //Create a new empty set just as we did at the previous exercise on Lists
        //Add few elements on this set and print the elements of the Set
        Set<String> myEmployeesSet1 = new HashSet<>();
        myEmployeesSet1.add("Samuel Smith");
        myEmployeesSet1.add("Bill Williams");
        myEmployeesSet1.add("Anthony Johnson");
        myEmployeesSet1.add("Cartner Caragher");
        System.out.println("MyEmployeesSet1: " + myEmployeesSet1.toString());

        //Create a list and add some elements
        List list = new ArrayList<String>();
        list.add("Astrid Conner");
        list.add("Christopher Adams");
        list.add("Antoine Griezmann");
        list.add("Adam Sandler");
        list.add("Bailey Aidan");
        list.add("Carl Edwin");
        list.add("Carl Edwin");

        //Now create the set using the appropriate constructor(in this case it’s going to be list)
        Set<String> myEmployeesSet2 = new HashSet<>(list);

        //Print the elements of the list and from the set
        System.out.println("list: " + list.toString());
        System.out.println("MyEmployeesSet2: " + myEmployeesSet2.toString());

        //Compare the two sets using equals() method
        System.out.println("myEmployeesSet1 matches myEmployeesSet2: " + myEmployeesSet1.equals(myEmployeesSet2));

        //Now we will remove one element from second set and compare again
        myEmployeesSet1.remove("Carl Edwin");
        System.out.println("MyEmployeesSet2: " + myEmployeesSet2.toString());
        System.out.println("myEmployeesSet1 matches myEmployeesSet2: " + myEmployeesSet1.equals(myEmployeesSet2));

        //Check if your sets contain all the elements of the list
        System.out.println("myEmployeesSet1 contains all the elements: " + myEmployeesSet1.equals(list));
        System.out.println("myEmployeesSet2 contains all the elements: " + myEmployeesSet2.equals(list));

        //Use the iterator in set and it will return true if the iteration has more elements
        Iterator iterator = myEmployeesSet1.iterator();
        while (iterator.hasNext()) {
            System.out.println("Iterator loop: " + iterator.next());
        }

        //Clear all the elements from the set using clear() method and print it
        myEmployeesSet1.clear();
        System.out.println("MyEmployeesSet1 is empty: " + myEmployeesSet1.isEmpty());

        //Check the number of elements from sets and print it
        System.out.println("myEmployeesSet1 has: " + myEmployeesSet1.size() + " Elements");
        System.out.println("myEmployeesSet2 has: " + myEmployeesSet2.size() + " Elements");

        //Create an Array with the contents of the Set and print it
        String[] myArray = myEmployeesSet2.toArray(new String[myEmployeesSet1.size()]);
        System.out.println("The array: " + Arrays.toString(myArray));

    }
}
